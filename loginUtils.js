// loginUtils.js

function checkLogin(usersData) {
    var email = document.getElementById("email").value;
    var nim = document.getElementById("nim").value;

    var found = false;
    for (var i = 0; i < usersData.length; i++) {
        if (usersData[i].email === email && usersData[i].nim === nim) {
            found = true;
            break;
        }
    }

    if (found) {
        localStorage.setItem('isLoggedIn', 'true');
        window.location.href = "undangan.html";
    } else {
        alert("Email atau NIM salah. Silakan coba lagi.");
    }
}
